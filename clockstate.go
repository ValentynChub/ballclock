package main

/*
ClockBState - clock ball state
Min  		1 to 4
FiveMin 	11 == 55 mins
Hour 		11
Queue 		its a queue for all balls
*/
type ClockBState struct {
	Min     []int
	FiveMin []int
	Hour    []int
	Queue   []int
}

//Init create new clock ball
func (cs *ClockBState) Init(nq int) {
	cs.Min = []int{}
	cs.FiveMin = []int{}
	cs.Hour = []int{}
	cs.Queue = []int{}

	for i := 1; i <= nq; i++ {
		cs.Queue = append(cs.Queue, i)
	}
}

//AddMin add ball to the min
func (cs *ClockBState) AddMin(nb int) {
	if len(cs.Min) == 4 {
		reverseQ(cs.Min)
		rq := cs.Min
		cs.Min = []int{}
		cs.Queue = append(cs.Queue, rq...)
		cs.AddFiveMin(nb)
	} else {
		cs.Min = append(cs.Min, nb)
	}
}

//AddFiveMin add ball to the fivemin
func (cs *ClockBState) AddFiveMin(nb int) {
	if len(cs.FiveMin) == 11 {
		reverseQ(cs.FiveMin)
		rq := cs.FiveMin
		cs.FiveMin = []int{}
		cs.Queue = append(cs.Queue, rq...)
		cs.AddHour(nb)
	} else {
		cs.FiveMin = append(cs.FiveMin, nb)
	}
}

//AddHour add ball to the hour
func (cs *ClockBState) AddHour(nb int) {
	if len(cs.Hour) == 11 {
		reverseQ(cs.Hour)
		rq := cs.Hour
		cs.Queue = append(cs.Queue, rq...)
		cs.Queue = append(cs.Queue, nb)
		cs.Hour = []int{}
	} else {
		cs.Hour = append(cs.Hour, nb)
	}
}

//GetBallFromQ get new ball from Queue
func (cs *ClockBState) GetBallFromQ() int {
	rb := cs.Queue[0]
	cs.Queue = dellBallQ(cs.Queue, 0)
	return rb
}

//AddBallFromQ -- increase ball in the min state
func (cs *ClockBState) AddBallFromQ() {
	currentBall := cs.GetBallFromQ()
	cs.AddMin(currentBall)
}

//DelBallQ delete first item from queue
func dellBallQ(queue []int, index int) []int {
	return append(queue[:index], queue[index+1:]...)
}

//ReverseQueue reverse queue array
func reverseQ(q []int) {
	l := len(q) - 1
	for i := l / 2; i >= 0; i-- {
		q[i], q[l-i] = q[l-i], q[i]
	}
}
