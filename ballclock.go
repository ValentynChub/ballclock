package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"reflect"
)

const (
	MinBalls = 27 //should be 27 min balls for correct simulation work
	MaxBalls = 127
)

var (

	//flag params for programm init
	mode  *string
	balls *int
	tm    *int

	//clock states
	InitialClockState ClockBState
	ClockState        ClockBState
)

func init() {
	mode = flag.String("mode", "mode1", "modes: mode1, mode2 (default is mode1)")
	balls = flag.Int("balls", 27, "balls: min=27, max=127")
	tm = flag.Int("time", 30, "number of minutes to run for")
}

func main() {
	flag.Parse()

	if !reflect.DeepEqual(*mode, "mode1") && !reflect.DeepEqual(*mode, "mode2") {
		log.Printf("Error: incorrect mode type %v \n", *mode)
		return
	}

	if *balls < MinBalls || *balls > MaxBalls {
		log.Printf("Error: balls must be from %v to %v\n", MinBalls, MaxBalls)
		return
	}

	if reflect.DeepEqual(*mode, "mode1") {
		fmt.Printf("Starting mode: %v, balls: %v \n", *mode, *balls)
		ComputationMode1(*balls)
	}

	if reflect.DeepEqual(*mode, "mode2") {
		fmt.Printf("Starting mode: %v, balls: %v, time:%v \n", *mode, *balls, *tm)
		ComputationMode2(*balls, *tm)
	}

}

func ComputationMode1(b int) {
	mins := 1
	days := 0

	ClockState.Init(b)
	InitialClockState.Init(b)

	ClockState.AddBallFromQ() //added first ball

	for !reflect.DeepEqual(InitialClockState, ClockState) {
		ClockState.AddBallFromQ()
		mins++
	}
	days = mins / (60 * 24)
	fmt.Printf("%v balls cycle after %v days.\n", b, days)
}

func ComputationMode2(b int, m int) {
	ClockState.Init(b)
	for i := 0; i < m; i++ {
		ClockState.AddBallFromQ()
	}

	jClockState, err := json.Marshal(ClockState)
	if err != nil {
		log.Panic(err)
	}
	fmt.Printf("%s\n", jClockState)
}
